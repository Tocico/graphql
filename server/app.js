const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');

const app = express();

// connect to mlab database
// make sure to replace my db string & creds with your own
const MongoClient = require('mongoose');
const uri = "mongodb+srv://mongo1045:mongo1045@gql.dwquo.mongodb.net/graphql?retryWrites=true&w=majority";
const assert = require('assert');
// const client = new MongoClient(uri, { useNewUrlParser: true });
const connectOption = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

/**
 * データベース接続
 * データベース接続用の引数追加
 */
MongoClient.connect(uri, connectOption, (err, client) => {

  /* Errorがあれば処理を中断 */
  assert.equal(null, err);

  /* 接続に成功すればコンソールに表示 */
  console.log('Connected successfully to server');

  /** DBを取得 */
  const db = client.db('graphql');

  /* DBとの接続切断 */
  client.close();
});

// bind express with graphql
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => {
    console.log('now listening for requests on port 4000');
});